from ubuntu:18.04
run apt-get update
run apt-get install wget -y
workdir /opt
run wget http://mirrors.estointernet.in/apache/tomcat/tomcat-8/v8.5.34/bin/apache-tomcat-8.5.34.tar.gz
run gunzip apache-tomcat-8.5.34.tar.gz
run tar -xvf apache-tomcat-8.5.34.tar
expose 8080
run apt install openjdk-8-jdk -y
env JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
entrypoint ["/opt/apache-tomcat-8.5.34/bin/catalina.sh","run"]


